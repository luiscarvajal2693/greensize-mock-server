MOCK SERVER CREADO PARA MI CLARO REBRANDING

Para agregar funciones/servicios a los mocks se debe modificar el correspondiente mock1,2,etc.
Para Agregar nuevos mock se deben crear el mock#.js e integrar en el index.js para que sea llamado.
Se debe crear un archivo .js por cada mock que se necesite.
La function exports.mock1, exports.mock2, exports.mock..., corresponde a cada uno de los mocks existente,
el mock1 por ejemplo esta creado para claro rebranding
Este mock1, Consta de un condigo bastante sencillo e intuitivo, todas las const declaradas al comienzo
son los objectos/respuestas de los servicios que ya estan preestablecidos, se pueden agregar
todos los necesarios segun la necesidad.
La funcion "exports.mock..." es donde ingresa la peticion del front y alli llama el .js correspondiente (mock....js)
ingresando en exports.handler del archivo y de alli se hace un ´switch case´
para identificar el method y de acuerdo al method devolver una respuesta. Se creo una funcion para
cada method, donde se puede adicionar logica de ser necesario. Incluir al ´switch case´ los 
caso de method necesarios y crear su funcion correspondiente para manejo del mismo.

pasos para hacer deployment

1* Tener instalado la console de npm y node.js
2* Si aun no se tienen los modulos del proyecto (lo cual sucede si se acaba de descarga el proyecto de gitlab),
 entrar en la carpeta functions, abrir una consola de comandos y correr el codigo
	npm isntall y npm install -g firebase-tools
3* Luego, abrir la console de comando en la carpeta del proyecto
4* usar el commando: 
	firebase login 
en caso de estar logeado ya con otro usuario que no tiene acceso al proyecto usar primero 
	firebase logout
y luego nuevamente
	firebase login
esto enviara a una web para autenticar con la cuenta de google 
5* Una vez logeado con una cuenta con acceso al proyecto, usar para actualizar el mock:
	firebase deploy --only functions

url del mock:
https://us-central1-mock-v2-4099f.cloudfunctions.net/mock3usfprincipal

