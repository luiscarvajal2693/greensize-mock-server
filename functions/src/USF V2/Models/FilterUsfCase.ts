import {Response} from "./responses";

export class FilterUsfCase extends Response{

    private _ErrorType: string | null;
    private _id: string | null;
    private _caseasine : any [];
    private _customercases:  any [];
    private _dataVendor: any [];
    private _pageNum: number;
    private _pageSize: number;
    private _totalRowCount: number;

    constructor(ErrorDesc: string | null, ErrorInterInfo: string | null, ErrorNum: number | null, HasError: boolean,
                ErrorType: string | null, id: string | null, caseasine: any[], customercases: any[], dataVendor: any[],
                pageNum: number, pageSize: number, totalRowCount: number) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._ErrorType = ErrorType;
        this._id = id;
        this._caseasine = caseasine;
        this._customercases = customercases;
        this._dataVendor = dataVendor;
        this._pageNum = pageNum;
        this._pageSize = pageSize;
        this._totalRowCount = totalRowCount;
    }

    get ErrorType(): string | null {
        return this._ErrorType;
    }

    set ErrorType(value: string | null) {
        this._ErrorType = value;
    }

    get id(): string | null {
        return this._id;
    }

    set id(value: string | null) {
        this._id = value;
    }

    get caseasine(): any[] {
        return this._caseasine;
    }

    set caseasine(value: any[]) {
        this._caseasine = value;
    }

    get customercases(): any[] {
        return this._customercases;
    }

    set customercases(value: any[]) {
        this._customercases = value;
    }

    get dataVendor(): any[] {
        return this._dataVendor;
    }

    set dataVendor(value: any[]) {
        this._dataVendor = value;
    }

    get pageNum(): number {
        return this._pageNum;
    }

    set pageNum(value: number) {
        this._pageNum = value;
    }

    get pageSize(): number {
        return this._pageSize;
    }

    set pageSize(value: number) {
        this._pageSize = value;
    }

    get totalRowCount(): number {
        return this._totalRowCount;
    }

    set totalRowCount(value: number) {
        this._totalRowCount = value;
    }



}

