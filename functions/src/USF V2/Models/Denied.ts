import {Response} from "./responses";

export class Denied extends Response {

    private _data: any[];

    constructor(ErrorDesc: string | null, ErrorInterInfo: string | null, ErrorNum: number | null, HasError: boolean,
                data: any[]) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._data = data;
    }


    get data(): any[] {
        return this._data;
    }

    set data(value: any[]) {
        this._data = value;
    }
}

