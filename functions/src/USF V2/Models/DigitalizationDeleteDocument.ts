
export class DigitalizationDeleteDocument {

    private _ErrorDesc: string;
    private _ErrorNum: number;
    private _ErrorType: string;
    private _HasError: boolean;
    private _id: string;
    private _data: Data [];
    private _error: any;
    private _response: boolean;
    private _result: string;


    constructor(ErrorDesc: string, ErrorNum: number, ErrorType: string, HasError: boolean, id: string, data: Data[], error: any, response: boolean, result: string) {
        this._ErrorDesc = ErrorDesc;
        this._ErrorNum = ErrorNum;
        this._ErrorType = ErrorType;
        this._HasError = HasError;
        this._id = id;
        this._data = data;
        this._error = error;
        this._response = response;
        this._result = result;
    }


    get ErrorDesc(): string {
        return this._ErrorDesc;
    }

    set ErrorDesc(value: string) {
        this._ErrorDesc = value;
    }

    get ErrorNum(): number {
        return this._ErrorNum;
    }

    set ErrorNum(value: number) {
        this._ErrorNum = value;
    }

    get ErrorType(): string {
        return this._ErrorType;
    }

    set ErrorType(value: string) {
        this._ErrorType = value;
    }

    get HasError(): boolean {
        return this._HasError;
    }

    set HasError(value: boolean) {
        this._HasError = value;
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get data(): Data[] {
        return this._data;
    }

    set data(value: Data[]) {
        this._data = value;
    }

    get error(): any {
        return this._error;
    }

    set error(value: any) {
        this._error = value;
    }

    get response(): boolean {
        return this._response;
    }

    set response(value: boolean) {
        this._response = value;
    }

    get result(): string {
        return this._result;
    }

    set result(value: string) {
        this._result = value;
    }

}

class Data {

    private _errorCode:string;
    private _message: string;
    private _success: string;

    constructor(errorCode: string, message: string, success: string) {
        this._errorCode = errorCode;
        this._message = message;
        this._success = success;
    }

    get errorCode(): string {
        return this._errorCode;
    }

    set errorCode(value: string) {
        this._errorCode = value;
    }

    get message(): string {
        return this._message;
    }

    set message(value: string) {
        this._message = value;
    }

    get success(): string {
        return this._success;
    }

    set success(value: string) {
        this._success = value;
    }
}
