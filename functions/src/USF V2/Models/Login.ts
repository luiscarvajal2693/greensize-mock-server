import {Response} from "./responses";

export class Login extends Response {


    private _Dealer: number;
    private _Active:boolean;
    private _DealerCode:string | null;
    private _Device_Id:string | null;
    private _Email: string | null;
    private _IsAdmin: boolean;
    private _LastLogin:string | null;
    private _LastName:string | null;
    private _Location:null;
    private _Locations: any[];
    private _LoginAttems:number;
    private _Name:string | null;
    private _Password: string|null;
    private _PhoneNumber:string | null;
    private _RoleID:number;
    private _RoleName:string | null;
    private _UserName:string | null;
    private _userid: number;

    constructor( ErrorDesc: string| null, ErrorInterInfo: string| null, ErrorNum: number,  HasError: boolean,
                 Dealer: number, Active:boolean,  DealerCode:string | null,  Device_Id:string|null, Email: string | null,
                 IsAdmin: boolean, LastLogin:string | null, LastName:string | null, Location:null | null, Locations: any[],
                 LoginAttems:number, Name:string | null, Password: string|null, PhoneNumber:string | null, RoleID:number,
                 RoleName:string | null, UserName:string | null, userid: number ) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._Dealer = Dealer;
        this._Active = Active;
        this._DealerCode = DealerCode;
        this._Device_Id = Device_Id;
        this._Email = Email;
        this._IsAdmin = IsAdmin;
        this._LastLogin = LastLogin;
        this._LastName = LastName;
        this._Location =Location;
        this._Locations = Locations;
        this._LoginAttems = LoginAttems;
        this._Name = Name;
        this._Password = Password;
        this._PhoneNumber = PhoneNumber;
        this._RoleID = RoleID;
        this._RoleName = RoleName;
        this._UserName = UserName;
        this._userid = userid;
    }

    get Dealer(): number {
        return this._Dealer;
    }

    set Dealer(value: number) {
        this._Dealer = value;
    }

    get Active(): boolean {
        return this._Active;
    }

    set Active(value: boolean) {
        this._Active = value;
    }

    get DealerCode(): string | null {
        return this._DealerCode;
    }

    set DealerCode(value: string | null) {
        this._DealerCode = value;
    }

    get Device_Id(): string | null {
        return this._Device_Id;
    }

    set Device_Id(value: string | null) {
        this._Device_Id = value;
    }

    get Email(): string | null {
        return this._Email;
    }

    set Email(value: string | null) {
        this._Email = value;
    }

    get IsAdmin(): boolean {
        return this._IsAdmin;
    }

    set IsAdmin(value: boolean) {
        this._IsAdmin = value;
    }

    get LastLogin(): string | null {
        return this._LastLogin;
    }

    set LastLogin(value: string | null) {
        this._LastLogin = value;
    }

    get LastName(): string | null {
        return this._LastName;
    }

    set LastName(value: string | null) {
        this._LastName = value;
    }

    get Location(): null {
        return this._Location;
    }

    set Location(value: null) {
        this._Location = value;
    }

    get Locations(): any[] {
        return this._Locations;
    }

    set Locations(value: any[]) {
        this._Locations = value;
    }

    get LoginAttems(): number {
        return this._LoginAttems;
    }

    set LoginAttems(value: number) {
        this._LoginAttems = value;
    }

    get Name(): string | null {
        return this._Name;
    }

    set Name(value: string | null) {
        this._Name = value;
    }

    get Password(): string | null {
        return this._Password;
    }

    set Password(value: string | null) {
        this._Password = value;
    }

    get PhoneNumber(): string | null {
        return this._PhoneNumber;
    }

    set PhoneNumber(value: string | null) {
        this._PhoneNumber = value;
    }

    get RoleID(): number {
        return this._RoleID;
    }

    set RoleID(value: number) {
        this._RoleID = value;
    }

    get RoleName(): string | null {
        return this._RoleName;
    }

    set RoleName(value: string | null) {
        this._RoleName = value;
    }

    get UserName(): string | null {
        return this._UserName;
    }

    set UserName(value: string | null) {
        this._UserName = value;
    }

    get userid(): number {
        return this._userid;
    }

    set userid(value: number) {
        this._userid = value;
    }


}
