export  class Response {
    private _ErrorDesc:string | null;
    private _ErrorInterInfo:string | null;
    private _ErrorNum:number | null;
    private _HasError: boolean;

    constructor(ErrorDesc: string | null, ErrorInterInfo: string| null, ErrorNum: number| null, HasError: boolean) {
        this._ErrorDesc = ErrorDesc;
        this._ErrorInterInfo = ErrorInterInfo;
        this._ErrorNum = ErrorNum;
        this._HasError = HasError;
    }

    get ErrorDesc(): string | null {
        return this._ErrorDesc;
    }

    set ErrorDesc(value: string | null) {
        this._ErrorDesc = value;
    }

    get ErrorInterInfo(): string | null {
        return this._ErrorInterInfo;
    }

    set ErrorInterInfo(value: string | null) {
        this._ErrorInterInfo = value;
    }

    get ErrorNum(): number | null {
        return this._ErrorNum;
    }

    set ErrorNum(value: number | null) {
        this._ErrorNum = value;
    }

    get HasError(): boolean {
        return this._HasError;
    }

    set HasError(value: boolean) {
        this._HasError = value;
    }
}
