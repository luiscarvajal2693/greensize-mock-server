export class GetLocationMcapi  {

    private _getLocationMcapi: Location [];

    constructor(getLocationMcapi: Location []) {
        this._getLocationMcapi = getLocationMcapi;
    }
    get getLocationMcapi(): Location[] {
        return this._getLocationMcapi;
    }

    set getLocationMcapi(value: Location[]) {
        this._getLocationMcapi = value;
    }
}

class Location {

    private _description_location: string;
    private _localidad_ID: number;
    private _location_sif: string;

    constructor(description_location: string, localidad_ID: number, location_sif: string) {
        this._description_location = description_location;
        this._localidad_ID = localidad_ID;
        this._location_sif = location_sif;
    }
    get description_location(): string {
        return this._description_location;
    }

    set description_location(value: string) {
        this._description_location = value;
    }

    get localidad_ID(): number {
        return this._localidad_ID;
    }

    set localidad_ID(value: number) {
        this._localidad_ID = value;
    }

    get location_sif(): string {
        return this._location_sif;
    }

    set location_sif(value: string) {
        this._location_sif = value;
    }
}

