export class PersonalDates  {

    private _ErrorDesc:string;
    private _ErrorNum: number;
    private _ErrorType:string;
    private _HasError: boolean;
    private _id: string;
    private _CASENUMBER: number;
    private _data: any[];
    private _dataObject: any[];
    private _error: string;
    private _response: boolean;
    private _result:string;

    constructor(ErrorDesc: string, ErrorNum: number, ErrorType: string, HasError: boolean, id: string, CASENUMBER: number, data: any[], dataObject: any[], error: string, response: boolean, result: string) {
        this._ErrorDesc = ErrorDesc;
        this._ErrorNum = ErrorNum;
        this._ErrorType = ErrorType;
        this._HasError = HasError;
        this._id = id;
        this._CASENUMBER = CASENUMBER;
        this._data = data;
        this._dataObject = dataObject;
        this._error = error;
        this._response = response;
        this._result = result;
    }

    get ErrorDesc(): string {
        return this._ErrorDesc;
    }

    set ErrorDesc(value: string) {
        this._ErrorDesc = value;
    }

    get ErrorNum(): number {
        return this._ErrorNum;
    }

    set ErrorNum(value: number) {
        this._ErrorNum = value;
    }

    get ErrorType(): string {
        return this._ErrorType;
    }

    set ErrorType(value: string) {
        this._ErrorType = value;
    }

    get HasError(): boolean {
        return this._HasError;
    }

    set HasError(value: boolean) {
        this._HasError = value;
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get CASENUMBER(): number {
        return this._CASENUMBER;
    }

    set CASENUMBER(value: number) {
        this._CASENUMBER = value;
    }

    get data(): any[] {
        return this._data;
    }

    set data(value: any[]) {
        this._data = value;
    }

    get dataObject(): any[] {
        return this._dataObject;
    }

    set dataObject(value: any[]) {
        this._dataObject = value;
    }

    get error(): string {
        return this._error;
    }

    set error(value: string) {
        this._error = value;
    }

    get response(): boolean {
        return this._response;
    }

    set response(value: boolean) {
        this._response = value;
    }

    get result(): string {
        return this._result;
    }

    set result(value: string) {
        this._result = value;
    }
}
