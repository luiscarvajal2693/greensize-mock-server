import {Response} from "./responses";

export class CreateDepend extends Response {

    private _id: string;

    constructor(ErrorDesc: string, ErrorInterInfo: string, ErrorNum: number, HasError: boolean, id: string) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._id = id;
    }
    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }
}
