export class DigitalizationUploadDocument  {

    private _error: any;
    private _response: boolean;
    private _result: string;
    private _keywords: any[];
    private _data: Data[];
    private _HasError: boolean;
    private _ErrorDesc:string;
    private _ErrorType: string;
    private _id:string;
    private _ErrorNum: number;

    constructor(error: any, response: boolean, result: string, keywords: any[], data: Data[], HasError: boolean, ErrorDesc: string, ErrorType: string, id: string, ErrorNum: number) {
        this._error = error;
        this._response = response;
        this._result = result;
        this._keywords = keywords;
        this._data = data;
        this._HasError = HasError;
        this._ErrorDesc = ErrorDesc;
        this._ErrorType = ErrorType;
        this._id = id;
        this._ErrorNum = ErrorNum;
    }

    get error(): any {
        return this._error;
    }

    set error(value: any) {
        this._error = value;
    }

    get response(): boolean {
        return this._response;
    }

    set response(value: boolean) {
        this._response = value;
    }

    get result(): string {
        return this._result;
    }

    set result(value: string) {
        this._result = value;
    }

    get keywords(): any[] {
        return this._keywords;
    }

    set keywords(value: any[]) {
        this._keywords = value;
    }

    get data(): Data[] {
        return this._data;
    }

    set data(value: Data[]) {
        this._data = value;
    }

    get HasError(): boolean {
        return this._HasError;
    }

    set HasError(value: boolean) {
        this._HasError = value;
    }

    get ErrorDesc(): string {
        return this._ErrorDesc;
    }

    set ErrorDesc(value: string) {
        this._ErrorDesc = value;
    }

    get ErrorType(): string {
        return this._ErrorType;
    }

    set ErrorType(value: string) {
        this._ErrorType = value;
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get ErrorNum(): number {
        return this._ErrorNum;
    }

    set ErrorNum(value: number) {
        this._ErrorNum = value;
    }
}

class Data {

    private _success: string;
    private _message:string;
    private _errorCode: string;
    private _Document: string;
    private _docPopURL: string;
    private _documentID: string;

    constructor(success: string, message: string, errorCode: string, Document: string, docPopURL: string, documentID: string) {
        this._success = success;
        this._message = message;
        this._errorCode = errorCode;
        this._Document = Document;
        this._docPopURL = docPopURL;
        this._documentID = documentID;
    }

    get success(): string {
        return this._success;
    }

    set success(value: string) {
        this._success = value;
    }

    get message(): string {
        return this._message;
    }

    set message(value: string) {
        this._message = value;
    }

    get errorCode(): string {
        return this._errorCode;
    }

    set errorCode(value: string) {
        this._errorCode = value;
    }

    get Document(): string {
        return this._Document;
    }

    set Document(value: string) {
        this._Document = value;
    }

    get docPopURL(): string {
        return this._docPopURL;
    }

    set docPopURL(value: string) {
        this._docPopURL = value;
    }

    get documentID(): string {
        return this._documentID;
    }

    set documentID(value: string) {
        this._documentID = value;
    }
}
