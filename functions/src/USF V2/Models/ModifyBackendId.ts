import {Response} from "./responses";

export class ModifyBackendId extends Response {

    private _dataBacken: any [];
    private _pageNum: number;
    private _pageSize: number;
    private _totalRowCount: number;

    constructor(ErrorDesc: string | null, ErrorInterInfo: string | null, ErrorNum: number | null, HasError: boolean,
                dataBacken: any[], pageNum: number, pageSize: number, totalRowCount: number) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._dataBacken = dataBacken;
        this._pageNum = pageNum;
        this._pageSize = pageSize;
        this._totalRowCount = totalRowCount;
    }




    get dataBacken(): any[] {
        return this._dataBacken;
    }

    set dataBacken(value: any[]) {
        this._dataBacken = value;
    }

    get pageNum(): number {
        return this._pageNum;
    }

    set pageNum(value: number) {
        this._pageNum = value;
    }

    get pageSize(): number {
        return this._pageSize;
    }

    set pageSize(value: number) {
        this._pageSize = value;
    }

    get totalRowCount(): number {
        return this._totalRowCount;
    }

    set totalRowCount(value: number) {
        this._totalRowCount = value;
    }
}

/**
 * class DataBackend {
    private _ACCOUNT_NUMBER: string;
    private _ADDRESS_1: string;
    private _ADDRESS_2: string;
    private _ASSIGNED_TO: string;
    private _AddValid: string;
    private _CITY: string;
    private _COUNTRY: string;
    private _CUSTOMER_DOB: string;
    private _CUSTOMER_LAST: string;
    private _CUSTOMER_MN: string;
    private _CUSTOMER_NAME: string;
    private _CUSTOMER_SSN: string;
    private _CUSTOMER_SSNMAX: string;
    private _DTS_CREATED: string;
    private _LOCATION: string;
    private _PHONE_1: string;
    private _SSN_VALIDATION: string;
    private _STATE:  string;
    private _SUSBCRIBER:  string;
    private _USF_CASEID : string;
    private _ZIPCODE: string;
    private _activeSubscriber: string;
    private _applicationId: string;
    private _eligibilityCheckId: string;
    private _eligibilityExpirationDate: string;
    private _validation_status: string;

    constructor(ACCOUNT_NUMBER: string, ADDRESS_1: string, ADDRESS_2: string, ASSIGNED_TO: string,
                AddValid: string, CITY: string, COUNTRY: string, CUSTOMER_DOB: string,
                CUSTOMER_LAST: string, CUSTOMER_MN: string, CUSTOMER_NAME: string, CUSTOMER_SSN: string,
                CUSTOMER_SSNMAX: string, DTS_CREATED: string, LOCATION: string, PHONE_1: string,
                SSN_VALIDATION: string, STATE:  string, SUSBCRIBER:  string, USF_CASEID : string,
                ZIPCODE: string, activeSubscriber: string, applicationId: string, eligibilityCheckId: string,
                eligibilityExpirationDate: string, validation_status: string){
                this._ACCOUNT_NUMBER =  ACCOUNT_NUMBER;
                this._ADDRESS_1 = ADDRESS_1;
                this._ADDRESS_2 = ADDRESS_2;
                this._ASSIGNED_TO = ASSIGNED_TO;
                this._AddValid = AddValid;
                this._CITY = CITY;
                this._COUNTRY = COUNTRY;
                this._CUSTOMER_DOB = CUSTOMER_DOB;
                this._CUSTOMER_LAST = CUSTOMER_LAST;
                this._CUSTOMER_MN = CUSTOMER_MN;
                this._CUSTOMER_NAME = CUSTOMER_NAME;
                this._CUSTOMER_SSN = CUSTOMER_SSN;
                this._CUSTOMER_SSNMAX =  CUSTOMER_SSNMAX;
                this._DTS_CREATED = DTS_CREATED;
                this._LOCATION = LOCATION;
                this._PHONE_1 = PHONE_1;
                this._SSN_VALIDATION = SSN_VALIDATION;
                this._STATE = STATE;
                this._SUSBCRIBER =  SUSBCRIBER;
                this._USF_CASEID = USF_CASEID;
                this._ZIPCODE = ZIPCODE;
                this._activeSubscriber = activeSubscriber;
                this._applicationId = applicationId;
                this._eligibilityCheckId = eligibilityCheckId;
                this._eligibilityExpirationDate = eligibilityExpirationDate;
                this._validation_status = validation_status;

    }

    get ACCOUNT_NUMBER(): string {
        return this._ACCOUNT_NUMBER;
    }

    set ACCOUNT_NUMBER(value: string) {
        this._ACCOUNT_NUMBER = value;
    }

    get ADDRESS_1(): string {
        return this._ADDRESS_1;
    }

    set ADDRESS_1(value: string) {
        this._ADDRESS_1 = value;
    }

    get ADDRESS_2(): string {
        return this._ADDRESS_2;
    }

    set ADDRESS_2(value: string) {
        this._ADDRESS_2 = value;
    }

    get ASSIGNED_TO(): string {
        return this._ASSIGNED_TO;
    }

    set ASSIGNED_TO(value: string) {
        this._ASSIGNED_TO = value;
    }

    get AddValid(): string {
        return this._AddValid;
    }

    set AddValid(value: string) {
        this._AddValid = value;
    }

    get CITY(): string {
        return this._CITY;
    }

    set CITY(value: string) {
        this._CITY = value;
    }

    get COUNTRY(): string {
        return this._COUNTRY;
    }

    set COUNTRY(value: string) {
        this._COUNTRY = value;
    }

    get CUSTOMER_DOB(): string {
        return this._CUSTOMER_DOB;
    }

    set CUSTOMER_DOB(value: string) {
        this._CUSTOMER_DOB = value;
    }

    get CUSTOMER_LAST(): string {
        return this._CUSTOMER_LAST;
    }

    set CUSTOMER_LAST(value: string) {
        this._CUSTOMER_LAST = value;
    }

    get CUSTOMER_MN(): string {
        return this._CUSTOMER_MN;
    }

    set CUSTOMER_MN(value: string) {
        this._CUSTOMER_MN = value;
    }

    get CUSTOMER_NAME(): string {
        return this._CUSTOMER_NAME;
    }

    set CUSTOMER_NAME(value: string) {
        this._CUSTOMER_NAME = value;
    }

    get CUSTOMER_SSN(): string {
        return this._CUSTOMER_SSN;
    }

    set CUSTOMER_SSN(value: string) {
        this._CUSTOMER_SSN = value;
    }

    get CUSTOMER_SSNMAX(): string {
        return this._CUSTOMER_SSNMAX;
    }

    set CUSTOMER_SSNMAX(value: string) {
        this._CUSTOMER_SSNMAX = value;
    }

    get DTS_CREATED(): string {
        return this._DTS_CREATED;
    }

    set DTS_CREATED(value: string) {
        this._DTS_CREATED = value;
    }

    get LOCATION(): string {
        return this._LOCATION;
    }

    set LOCATION(value: string) {
        this._LOCATION = value;
    }

    get PHONE_1(): string {
        return this._PHONE_1;
    }

    set PHONE_1(value: string) {
        this._PHONE_1 = value;
    }

    get SSN_VALIDATION(): string {
        return this._SSN_VALIDATION;
    }

    set SSN_VALIDATION(value: string) {
        this._SSN_VALIDATION = value;
    }

    get STATE(): string {
        return this._STATE;
    }

    set STATE(value: string) {
        this._STATE = value;
    }

    get SUSBCRIBER(): string {
        return this._SUSBCRIBER;
    }

    set SUSBCRIBER(value: string) {
        this._SUSBCRIBER = value;
    }

    get USF_CASEID(): string {
        return this._USF_CASEID;
    }

    set USF_CASEID(value: string) {
        this._USF_CASEID = value;
    }

    get ZIPCODE(): string {
        return this._ZIPCODE;
    }

    set ZIPCODE(value: string) {
        this._ZIPCODE = value;
    }

    get activeSubscriber(): string {
        return this._activeSubscriber;
    }

    set activeSubscriber(value: string) {
        this._activeSubscriber = value;
    }

    get applicationId(): string {
        return this._applicationId;
    }

    set applicationId(value: string) {
        this._applicationId = value;
    }

    get eligibilityCheckId(): string {
        return this._eligibilityCheckId;
    }

    set eligibilityCheckId(value: string) {
        this._eligibilityCheckId = value;
    }

    get eligibilityExpirationDate(): string {
        return this._eligibilityExpirationDate;
    }

    set eligibilityExpirationDate(value: string) {
        this._eligibilityExpirationDate = value;
    }

    get validation_status(): string {
        return this._validation_status;
    }

    set validation_status(value: string) {
        this._validation_status = value;
    }
}
 */
