import {Response} from "./responses";

export class EligibilitycheckStatusMcapi extends Response {

    private _AddValid:boolean;
    private _links: any;
    private _activeSubscriber: string | null;
    private _applicationId:string | null;
    private _eligibilityCheckId:string | null;
    private _eligibilityExpirationDate:string | null;
    private _errorId:string | null;
    private _errors:any[];
    private _failures:any[];
    private _lastManualReviewTime:string | null;
    private _message:string | null;
    private _numberofManualReview:string | null;
    private _rejections:any[];
    private _resoncode:number;
    private _status:string | null;
    private _timestamp:string | null;

    constructor(ErrorDesc: string | null, ErrorInterInfo: string | null, ErrorNum: number | null, HasError: boolean,
                AddValid: boolean, links: any, activeSubscriber: string | null, applicationId: string | null,
                eligibilityCheckId: string | null, eligibilityExpirationDate: string | null, errorId: string | null,
                errors: any[], failures: any[], lastManualReviewTime: string | null, message: string | null,
                numberofManualReview: string | null, rejections: any[], resoncode: number, status: string | null,
                timestamp: string | null) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._AddValid = AddValid;
        this._links = links;
        this._activeSubscriber = activeSubscriber;
        this._applicationId = applicationId;
        this._eligibilityCheckId = eligibilityCheckId;
        this._eligibilityExpirationDate = eligibilityExpirationDate;
        this._errorId = errorId;
        this._errors = errors;
        this._failures = failures;
        this._lastManualReviewTime = lastManualReviewTime;
        this._message = message;
        this._numberofManualReview = numberofManualReview;
        this._rejections = rejections;
        this._resoncode = resoncode;
        this._status = status;
        this._timestamp = timestamp;
    }

    get AddValid(): boolean {
        return this._AddValid;
    }

    set AddValid(value: boolean) {
        this._AddValid = value;
    }

    get links(): any {
        return this._links;
    }

    set links(value: any) {
        this._links = value;
    }

    get activeSubscriber(): string | null {
        return this._activeSubscriber;
    }

    set activeSubscriber(value: string | null) {
        this._activeSubscriber = value;
    }

    get applicationId(): string | null {
        return this._applicationId;
    }

    set applicationId(value: string | null) {
        this._applicationId = value;
    }

    get eligibilityCheckId(): string | null {
        return this._eligibilityCheckId;
    }

    set eligibilityCheckId(value: string | null) {
        this._eligibilityCheckId = value;
    }

    get eligibilityExpirationDate(): string | null {
        return this._eligibilityExpirationDate;
    }

    set eligibilityExpirationDate(value: string | null) {
        this._eligibilityExpirationDate = value;
    }

    get errorId(): string | null {
        return this._errorId;
    }

    set errorId(value: string | null) {
        this._errorId = value;
    }

    get errors(): any[] {
        return this._errors;
    }

    set errors(value: any[]) {
        this._errors = value;
    }

    get failures(): any[] {
        return this._failures;
    }

    set failures(value: any[]) {
        this._failures = value;
    }

    get lastManualReviewTime(): string | null {
        return this._lastManualReviewTime;
    }

    set lastManualReviewTime(value: string | null) {
        this._lastManualReviewTime = value;
    }

    get message(): string | null {
        return this._message;
    }

    set message(value: string | null) {
        this._message = value;
    }

    get numberofManualReview(): string | null {
        return this._numberofManualReview;
    }

    set numberofManualReview(value: string | null) {
        this._numberofManualReview = value;
    }

    get rejections(): any[] {
        return this._rejections;
    }

    set rejections(value: any[]) {
        this._rejections = value;
    }

    get resoncode(): number {
        return this._resoncode;
    }

    set resoncode(value: number) {
        this._resoncode = value;
    }

    get status(): string | null {
        return this._status;
    }

    set status(value: string | null) {
        this._status = value;
    }

    get timestamp(): string | null {
        return this._timestamp;
    }

    set timestamp(value: string | null) {
        this._timestamp = value;
    }




}


