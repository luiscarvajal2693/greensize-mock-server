import {Response} from "./responses";

export class GetBan extends Response {

    private _CUSTOMER_LAST: string;
    private _CUSTOMER_MN: string;
    private _CUSTOMER_NAME: string;
    private _USF_CASEID: string;
    private _mBan: string;
    private _subscriber:string;

    constructor(ErrorDesc: string, ErrorInterInfo: string, ErrorNum: number, HasError: boolean, CUSTOMER_LAST: string, CUSTOMER_MN: string, CUSTOMER_NAME: string, USF_CASEID: string, mBan: string, subscriber: string) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._CUSTOMER_LAST = CUSTOMER_LAST;
        this._CUSTOMER_MN = CUSTOMER_MN;
        this._CUSTOMER_NAME = CUSTOMER_NAME;
        this._USF_CASEID = USF_CASEID;
        this._mBan = mBan;
        this._subscriber = subscriber;
    }

    get CUSTOMER_LAST(): string {
        return this._CUSTOMER_LAST;
    }

    set CUSTOMER_LAST(value: string) {
        this._CUSTOMER_LAST = value;
    }

    get CUSTOMER_MN(): string {
        return this._CUSTOMER_MN;
    }

    set CUSTOMER_MN(value: string) {
        this._CUSTOMER_MN = value;
    }

    get CUSTOMER_NAME(): string {
        return this._CUSTOMER_NAME;
    }

    set CUSTOMER_NAME(value: string) {
        this._CUSTOMER_NAME = value;
    }

    get USF_CASEID(): string {
        return this._USF_CASEID;
    }

    set USF_CASEID(value: string) {
        this._USF_CASEID = value;
    }

    get mBan(): string {
        return this._mBan;
    }

    set mBan(value: string) {
        this._mBan = value;
    }

    get subscriber(): string {
        return this._subscriber;
    }

    set subscriber(value: string) {
        this._subscriber = value;
    }
}
