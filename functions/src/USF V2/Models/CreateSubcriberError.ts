import {Response} from "./responses";

export class CreateSubcriberError extends Response{

    private _mBan: string;
    private _subscriber: string;

    constructor(ErrorDesc: string, ErrorInterInfo: string, ErrorNum: number,  HasError: boolean,
                mBan: string, subscriber: string){
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._mBan = mBan;
        this._subscriber = subscriber;
    }

    get mBan(): string {
        return this._mBan;
    }

    set mBan(value: string) {
        this._mBan = value;
    }

    get subscriber(): string {
        return this._subscriber;
    }

    set subscriber(value: string) {
        this._subscriber = value;
    }
}
