
import {Response} from "./responses";

export class UsfVerification extends Response {

    private _Errordisplay: string;
    private _AddValid: boolean;
    private _links: Link;
    private _activeSubscriber: string;
    private _applicationId:  string;
    private _eligibilityCheckId: string;
    private _eligibilityExpirationDate: string;
    private _errorId: string;
    private _errors: any[];
    private _failures: string[];
    private _lastManualReviewTime: string;
    private _message: string;
    private _numberofManualReview: string;
    private _rejections: any[];
    private _resoncode: number;
    private _status: string;
    private _timestamp: string;

    constructor(ErrorDesc: string, ErrorInterInfo: string, ErrorNum: number, HasError: boolean, Errordisplay: string, AddValid: boolean, links: Link, activeSubscriber: string, applicationId: string, eligibilityCheckId: string, eligibilityExpirationDate: string, errorId: string, errors: any[], failures: string[], lastManualReviewTime: string, message: string, numberofManualReview: string, rejections: any[], resoncode: number, status: string, timestamp: string) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._Errordisplay = Errordisplay;
        this._AddValid = AddValid;
        this._links = links;
        this._activeSubscriber = activeSubscriber;
        this._applicationId = applicationId;
        this._eligibilityCheckId = eligibilityCheckId;
        this._eligibilityExpirationDate = eligibilityExpirationDate;
        this._errorId = errorId;
        this._errors = errors;
        this._failures = failures;
        this._lastManualReviewTime = lastManualReviewTime;
        this._message = message;
        this._numberofManualReview = numberofManualReview;
        this._rejections = rejections;
        this._resoncode = resoncode;
        this._status = status;
        this._timestamp = timestamp;
    }

    get Errordisplay(): string {
        return this._Errordisplay;
    }

    set Errordisplay(value: string) {
        this._Errordisplay = value;
    }

    get AddValid(): boolean {
        return this._AddValid;
    }

    set AddValid(value: boolean) {
        this._AddValid = value;
    }

    get links(): Link {
        return this._links;
    }

    set links(value: Link) {
        this._links = value;
    }

    get activeSubscriber(): string {
        return this._activeSubscriber;
    }

    set activeSubscriber(value: string) {
        this._activeSubscriber = value;
    }

    get applicationId(): string {
        return this._applicationId;
    }

    set applicationId(value: string) {
        this._applicationId = value;
    }

    get eligibilityCheckId(): string {
        return this._eligibilityCheckId;
    }

    set eligibilityCheckId(value: string) {
        this._eligibilityCheckId = value;
    }

    get eligibilityExpirationDate(): string {
        return this._eligibilityExpirationDate;
    }

    set eligibilityExpirationDate(value: string) {
        this._eligibilityExpirationDate = value;
    }

    get errorId(): string {
        return this._errorId;
    }

    set errorId(value: string) {
        this._errorId = value;
    }

    get errors(): any[] {
        return this._errors;
    }

    set errors(value: any[]) {
        this._errors = value;
    }

    get failures(): string[] {
        return this._failures;
    }

    set failures(value: string[]) {
        this._failures = value;
    }

    get lastManualReviewTime(): string {
        return this._lastManualReviewTime;
    }

    set lastManualReviewTime(value: string) {
        this._lastManualReviewTime = value;
    }

    get message(): string {
        return this._message;
    }

    set message(value: string) {
        this._message = value;
    }

    get numberofManualReview(): string {
        return this._numberofManualReview;
    }

    set numberofManualReview(value: string) {
        this._numberofManualReview = value;
    }

    get rejections(): any[] {
        return this._rejections;
    }

    set rejections(value: any[]) {
        this._rejections = value;
    }

    get resoncode(): number {
        return this._resoncode;
    }

    set resoncode(value: number) {
        this._resoncode = value;
    }

    get status(): string {
        return this._status;
    }

    set status(value: string) {
        this._status = value;
    }

    get timestamp(): string {
        return this._timestamp;
    }

    set timestamp(value: string) {
        this._timestamp = value;
    }

}


/**
 * Clase link necesaria para la clase UsfVerification
 * */
class Link {
    private _certification: Href;
    private _resolution: Href;
    constructor(certification: Href, resolution: Href ) {
        this._certification = certification;
        this._resolution = resolution;
    }

    get certification(): Href {
        return this._certification;
    }

    set certification(value: Href) {
        this._certification = value;
    }

    get resolution(): Href {
        return this._resolution;
    }

    set resolution(value: Href) {
        this._resolution = value;
    }
}

/**
 * Clase Href necesaria para la clase  Link
 * */
class Href {
    private _herf: string;

    constructor(herf: string){
        this._herf = herf;
    }

    get herf(): string {
        return this._herf;
    }

    set herf(value: string) {
        this._herf = value;
    }
}

