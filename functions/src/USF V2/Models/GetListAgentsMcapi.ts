import {Response} from "./responses";

export class GetLisAgentsMcapi extends Response{

    private _LIST_AGENT: string | null;
    private _data: any [];

    constructor( ErrorDesc: string | null, ErrorInterInfo: string| null, ErrorNum: number,  HasError: boolean,
                 LIST_AGENT: string | null, data: any []) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._LIST_AGENT = LIST_AGENT;
        this._data = data;
    }

    get LIST_AGENT(): string | null {
        return this._LIST_AGENT;
    }

    set LIST_AGENT(value: string | null) {
        this._LIST_AGENT = value;
    }

    get data(): any[] {
        return this._data;
    }

    set data(value: any[]) {
        this._data = value;
    }

}
