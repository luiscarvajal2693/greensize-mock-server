import {Response} from "./responses";

export class CreateAccount extends Response {

    private _AccountType: string;
    private _mBan: string;
    private _response: boolean;

    constructor( ErrorDesc: string, ErrorInterInfo: string, ErrorNum: number,  HasError: boolean,
                 AccountType: string, mBan: string, response: boolean ) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._AccountType = AccountType;
        this._mBan =  mBan;
        this._response = response;
    }

    get AccountType(): string {
        return this._AccountType;
    }

    set AccountType(value: string) {
        this._AccountType = value;
    }

    get mBan(): string {
        return this._mBan;
    }

    set mBan(value: string) {
        this._mBan = value;
    }

    get response(): boolean {
        return this._response;
    }

    set response(value: boolean) {
        this._response = value;
    }

}
