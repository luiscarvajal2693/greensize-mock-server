
export class AddresDate  {

    private _ErrorDesc: string;
    private _ErrorNum: number;
    private _HasError: boolean
    private _id:string;
    private _Response:boolean;
    private _data:any[];
    private _error:string;
    private _result:boolean;
    private _strnlad:any;
    private _dataObject: data[];

    constructor(ErrorDesc: string, ErrorNum: number, HasError: boolean, id: string, Response: boolean, data: any[], error: string, result: boolean, strnlad: any, dataObject: data[]) {
        this._ErrorDesc = ErrorDesc;
        this._ErrorNum = ErrorNum;
        this._HasError = HasError;
        this._id = id;
        this._Response = Response;
        this._data = data;
        this._error = error;
        this._result = result;
        this._strnlad = strnlad;
        this._dataObject = dataObject;
    }


    get ErrorDesc(): string {
        return this._ErrorDesc;
    }

    set ErrorDesc(value: string) {
        this._ErrorDesc = value;
    }

    get ErrorNum(): number {
        return this._ErrorNum;
    }

    set ErrorNum(value: number) {
        this._ErrorNum = value;
    }

    get HasError(): boolean {
        return this._HasError;
    }

    set HasError(value: boolean) {
        this._HasError = value;
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get Response(): boolean {
        return this._Response;
    }

    set Response(value: boolean) {
        this._Response = value;
    }

    get data(): any[] {
        return this._data;
    }

    set data(value: any[]) {
        this._data = value;
    }

    get error(): string {
        return this._error;
    }

    set error(value: string) {
        this._error = value;
    }

    get result(): boolean {
        return this._result;
    }

    set result(value: boolean) {
        this._result = value;
    }

    get strnlad(): any {
        return this._strnlad;
    }

    set strnlad(value: any) {
        this._strnlad = value;
    }

    get dataObject(): data[] {
        return this._dataObject;
    }

    set dataObject(value: data[]) {
        this._dataObject = value;
    }

}

class data {

    private _CASENUMBER:string;
    private _CUSTOMERADDRESS:string;
    private _CUSTOMERLASTNAME:string;
    private _CUSTOMERNAME:string;
    private _DOB:string;
    private _SSN:string;
    private _SUGGESTADDRESS:string;
    private _straddress:string;
    private _strcity:string;
    private _strstate:string;
    private _strurbanization:string;
    private _strzipcode:string;

    constructor(CASENUMBER: string, CUSTOMERADDRESS: string, CUSTOMERLASTNAME: string, CUSTOMERNAME: string, DOB: string, SSN: string, SUGGESTADDRESS: string, straddress: string, strcity: string, strstate: string, strurbanization: string, strzipcode: string) {
        this._CASENUMBER = CASENUMBER;
        this._CUSTOMERADDRESS = CUSTOMERADDRESS;
        this._CUSTOMERLASTNAME = CUSTOMERLASTNAME;
        this._CUSTOMERNAME = CUSTOMERNAME;
        this._DOB = DOB;
        this._SSN = SSN;
        this._SUGGESTADDRESS = SUGGESTADDRESS;
        this._straddress = straddress;
        this._strcity = strcity;
        this._strstate = strstate;
        this._strurbanization = strurbanization;
        this._strzipcode = strzipcode;
    }

    get CASENUMBER(): string {
        return this._CASENUMBER;
    }

    set CASENUMBER(value: string) {
        this._CASENUMBER = value;
    }

    get CUSTOMERADDRESS(): string {
        return this._CUSTOMERADDRESS;
    }

    set CUSTOMERADDRESS(value: string) {
        this._CUSTOMERADDRESS = value;
    }

    get CUSTOMERLASTNAME(): string {
        return this._CUSTOMERLASTNAME;
    }

    set CUSTOMERLASTNAME(value: string) {
        this._CUSTOMERLASTNAME = value;
    }

    get CUSTOMERNAME(): string {
        return this._CUSTOMERNAME;
    }

    set CUSTOMERNAME(value: string) {
        this._CUSTOMERNAME = value;
    }

    get DOB(): string {
        return this._DOB;
    }

    set DOB(value: string) {
        this._DOB = value;
    }

    get SSN(): string {
        return this._SSN;
    }

    set SSN(value: string) {
        this._SSN = value;
    }

    get SUGGESTADDRESS(): string {
        return this._SUGGESTADDRESS;
    }

    set SUGGESTADDRESS(value: string) {
        this._SUGGESTADDRESS = value;
    }

    get straddress(): string {
        return this._straddress;
    }

    set straddress(value: string) {
        this._straddress = value;
    }

    get strcity(): string {
        return this._strcity;
    }

    set strcity(value: string) {
        this._strcity = value;
    }

    get strstate(): string {
        return this._strstate;
    }

    set strstate(value: string) {
        this._strstate = value;
    }

    get strurbanization(): string {
        return this._strurbanization;
    }

    set strurbanization(value: string) {
        this._strurbanization = value;
    }

    get strzipcode(): string {
        return this._strzipcode;
    }

    set strzipcode(value: string) {
        this._strzipcode = value;
    }

}
