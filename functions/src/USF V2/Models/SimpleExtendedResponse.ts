import {Response} from "./responses";

export class SimpleExtendedResponse extends Response {

    private _response: boolean;

    constructor(ErrorDesc: string|null, ErrorInterInfo: string| null, ErrorNum: number| null, HasError: boolean, response: boolean) {
        super(ErrorDesc, ErrorInterInfo, ErrorNum, HasError);
        this._response = response;
    }


    get response(): boolean {
        return this._response;
    }

    set response(value: boolean) {
        this._response = value;
    }
}
