import { AceptationTerms, AccountCreation, Activation, AddressDate,
         DocumentDigitalization, PersonalDates, PreviewAndFirm, ResgiterCase,
         UsfVerification } from './functions/UniversalServices'
import {LoginAndHome} from "./functions/LoginAndHome";
import {UsfCases} from "./functions/usfCases";


exports.handler = function(request: any, response: any) {

    // UniversalServices:

    const aceptationTerms:AceptationTerms = new AceptationTerms();
    const accountCreation:AccountCreation = new AccountCreation();
    const activation:Activation = new Activation();
    const addressDate:AddressDate = new AddressDate();
    const documentDigitalization: DocumentDigitalization = new DocumentDigitalization();
    const personalDates: PersonalDates = new PersonalDates();
    const previewAndFirm:PreviewAndFirm = new PreviewAndFirm();
    const resgiterCase:ResgiterCase = new ResgiterCase();
    const usfVerification:UsfVerification = new UsfVerification();

    response.set('Access-Control-Allow-Origin', '*');

    if (request.method === 'OPTIONS') {
        // Send responseponse to OPTIONS requests
        response.set('Access-Control-Allow-Methods', 'POST');
        response.set('Access-Control-Allow-Headers', 'Content-Type');
        response.set('Access-Control-Max-Age', '3600');
        response.status(204).send('');
    } else {
        // Set CORS headers for the main request
        response.set('Access-Control-Allow-Origin', '*');
        // response.send('Hello World!');

        var body = request.body;
        switch (body.method) {
            case 'loginAdMcapi':
                LoginAndHome.loginAdMcapi(request, response);
                break;
            case 'getCasesWithFiltersMcapi':
                UsfCases.getCasesWithFiltersMcapi(request, response);
                break;
            case 'validareSSNAdMcapi':
                personalDates.validareSSNAdMcapi(request, response);
                break;
            case 'addressValidationMcapi':
                addressDate.addressValidationMcapi(request, response);
                break;
            case 'subscriberVerificationMcapi':
                addressDate.subscriberVerificationMcapi(request, response);
                break;
            case 'UpdloadDocumentMcapi':
                documentDigitalization.UpdloadDocumentMcapi(request, response);
                break;
            case 'DeleteDocumentMcapi':
                documentDigitalization.DeleteDocumentMcapi(request, response);
                break;
            case 'CreateNewAccountMcapi':
                accountCreation.CreateNewAccountMcapi(request, response);
                break;
            case 'CreateTerminosMcapi':
                aceptationTerms.CreateTerminosMcapi(request, response);
                break;
            case 'Updlongdistance':
                aceptationTerms.Updlongdistance(request, response);
                break;
            case 'CreatefirmMcapi':
                previewAndFirm.CreatefirmMcapi(request, response);
                break;
            case 'CreateSubscriberMcapi':
                accountCreation.CreateSubscriberMcapi(request, response);
                break;
            case 'getBanMcapi':
                activation.getBanMcapi(request, response);
                break;
            case 'getCasesdeniedMcapi':
                UsfCases.getCasesdeniedMcapi(request, response);
                break;
            case 'getCasesModifyBackendId':
                UsfCases.getCasesModifyBackendId(request, response);
                break;
            case 'getLocationMcapi':
                accountCreation.getLocationMcapi(request, response);
                break;
            case 'FccFormDocumentMcapi':
                previewAndFirm.fccFormDocumentMcapi(request, response);
                break;
            case 'GetListAgentMcapi':
                LoginAndHome.getListAgentMcapi(request, response);
                    break;
            case 'CreateDepend':
                usfVerification.createDepend(request, response);
                    break;
            case 'eligibilitycheckStatusMcapi':
                UsfCases.eligibilitycheckStatusMcapi(request, response);
                    break;
            case 'DeleteCaseAllMcapi':
                resgiterCase.deleteCaseAllMcapi(request, response);
                    break;

            default:
                response.status(404).send('Method no found');
        }
    }

};










    






