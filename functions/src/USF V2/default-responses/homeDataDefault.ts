export const GETLISTAGENTSMCAPI = {
    GETLISTAGENTSMCAPI_OK :{
        "ErrorDesc": null,
        "ErrorInterInfo": null,
        "ErrorNum": 0,
        "HasError": false,
        "LIST_AGENT": "",
        "data": [
            {
                "AGENT_DESCRIPCION": "Medicaid",
                "bit_str": "Y   ",
                "cod_id": "E1        "
            },
            {
                "AGENT_DESCRIPCION": "Supplemental Nutrition Assistance Program (SNAP)",
                "bit_str": "Y   ",
                "cod_id": "E2        "
            },
            {
                "AGENT_DESCRIPCION": "Federal Public Housing Assistance",
                "bit_str": "Y   ",
                "cod_id": "E4        "
            },
            {
                "AGENT_DESCRIPCION": "Veterans Pension or Survivors Pension Required",
                "bit_str": "Y   ",
                "cod_id": "E15       "
            }
        ]
    },
};
