import { ACEPTATION_TERMS } from './universalResponses/aceptation-terms';
import { ACCOUNT_CREATION } from './universalResponses/acount-creation';
import { ACTIVATION } from './universalResponses/activation';
import { ADDRESS_DATE } from './universalResponses/address-date';
import { DOCUMENT_DIGITALIZATION } from './universalResponses/document-digitalization';
import { PERSONAL_DATES } from './universalResponses/personal-dates';
import { PREVIEW_AND_FIRM } from './universalResponses/preview-view-and-firm';
import { REGISTER_CASE } from './universalResponses/registercase';
import { USF_VERIFICATION } from './universalResponses/usf-verification';

export  const UNIVERSALDEFAULTRESPONSE = {
    TERMS: ACEPTATION_TERMS,
    CREATION: ACCOUNT_CREATION,
    ACTIVATION: ACTIVATION,
    ADDRESS: ADDRESS_DATE,
    DOCUMENT: DOCUMENT_DIGITALIZATION,
    PERSONAL: PERSONAL_DATES,
    FIRM: PREVIEW_AND_FIRM,
    REGISTER: REGISTER_CASE,
    VERIFICATION: USF_VERIFICATION
};
