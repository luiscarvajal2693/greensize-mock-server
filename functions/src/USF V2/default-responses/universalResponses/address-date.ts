export const ADDRESS_DATE = {
    ADDRES_DATE :{
        "ErrorDesc":"",
        "ErrorNum":0,
        "ErrorType":"",
        "HasError":false,
        "id":"",
        "Response":true,
        "data":[],
        "dataObject":[
            {"CASENUMBER":"1835",
            "CUSTOMERADDRESS":"",
            "CUSTOMERLASTNAME":"FERRAZ",
            "CUSTOMERNAME":"JOHNNY",
            "DOB":"1/31/1998 12:00:00 AM",
            "SSN":"6789",
            "SUGGESTADDRESS":"180 Calle Jose F Diaz San Juan PR 00926-5972 "
            ,"straddress":"",
            "strcity":"",
            "strstate":"",
            "strurbanization":"",
            "strzipcode":""},
            {"CASENUMBER":"1835",
            "CUSTOMERADDRESS":"180 JOSE F DIAZ San Juan PR 00926",
            "CUSTOMERLASTNAME":"FERRAZ",
            "CUSTOMERNAME":"JOHNNY",
            "DOB":"1/31/1998 12:00:00 AM",
            "SSN":"6789",
            "SUGGESTADDRESS":"180 Calle Jose F Diaz San Juan PR 00926-5972 ",
            "straddress":"",
            "strcity":"",
            "strstate":"",
            "strurbanization":"",
            "strzipcode":""},
            {"CASENUMBER":"1835",
            "CUSTOMERADDRESS":"180 Calle Jose F Diaz San Juan PR 00926-5972 ",
            "CUSTOMERLASTNAME":"FERRAZ",
            "CUSTOMERNAME":"JOHNNY",
            "DOB":"1/31/1998 12:00:00 AM",
            "SSN":"6789",
            "SUGGESTADDRESS":"180 Calle Jose F Diaz San Juan PR 00926-5972 ",
            "straddress":"",
            "strcity":"",
            "strstate":"",
            "strurbanization":"",
            "strzipcode":""}],
        "error":"",
        "result":true,
        "strnlad":null
    },
    USF_VERIFICATION : {
        "ErrorDesc": null,
        "ErrorInterInfo": null,
        "ErrorNum": 0,
        "HasError": true,
        "Errordisplay": "The date 10272019 cannot be before than backdate limit 120419",
        "AddValid": false,
        "_links": {
            "certification": {
                "href": ""
            },
            "resolution": {
                "href": ""
            }
        },
        "activeSubscriber": "N",
        "applicationId": "Q55712-79714",
        "eligibilityCheckId": "E41153D1E9F0BBE7677D83D6C4F5146AC22ABE5518334CB61A740D6D5AF9AC46",
        "eligibilityExpirationDate": "2020-04-28",
        "errorId": "",
        "errors": [],
        "failures": [
            "TPIV_FAIL_DECEASED",
            "INVALID_ADDRESS"
        ],
        "lastManualReviewTime": "",
        "message": "",
        "numberofManualReview": "",
        "rejections": [],
        "resoncode": 6,
        "status": "COMPLETE",
        "timestamp": ""
    },

}


