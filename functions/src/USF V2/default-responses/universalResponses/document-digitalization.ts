export const DOCUMENT_DIGITALIZATION = {
    DIGITALIZATION_UPLOAD_DOCUMENT :{
        "error": null,
        "response": true,
        "result": "",
        "keywords": [],
        "data": [
            {
                "success": "true",
                "message": "Document created successful",
                "errorCode": "0",
                "Document": "",
                "docPopURL": "https://claropr.onbaseonline.com/1601AppNet/docpop/docpop.aspx?clienttype=html&docid=1057971&chksum=8c1212d567a0ba5bb387a132acea6d54ab165432d55100a992771cefe2d904bd",
                "documentID": "1057971"
            }
        ],
        "HasError": false,
        "ErrorDesc": "",
        "ErrorType": "",
        "id": "",
        "ErrorNum": 0
    },
    DIGITALIZATION_DELETE_DOCUMENT : {
        "ErrorDesc": "",
        "ErrorNum": 0,
        "ErrorType": "",
        "HasError": false,
        "id": "",
        "data": [
            {
                "errorCode": "0",
                "message": "Document deleted",
                "success": "true"
            }
        ],
        "error": null,
        "response": true,
        "result": ""
    },
    ERRORES_USF : {
        UpdloadDocumentMcapi:{
            "error": null,
            "response": true,
            "result": "",
            "keywords": [],
            "data": [
                {
                    "success": "false",
                    "message": "Error connecting to Onbase",
                    "errorCode": "101",
                    "Document": "",
                    "docPopURL": "",
                    "documentID": ""
                }
            ],
            "HasError": true,
            "ErrorDesc": "Estimado usuario por el momento la aplicacion no esta disponible para su uso por favor intentente nuevamente en 5 min o contacte al administrador del sitema",
            "ErrorType": "",
            "id": "",
            "ErrorNum": 0
        }
    },
}

