export const ACCOUNT_CREATION = {
    CREATE_SUBSCRIBER :{
        "ErrorDesc": null,
        "ErrorInterInfo": null,
        "ErrorNum": 0,
        "HasError": false,
        "CUSTOMER_LAST": "RAMOS MARTINEZ",
        "CUSTOMER_MN": "",
        "CUSTOMER_NAME": "JUAN",
        "USF_CASEID": "509",
        "mBan": "790870312",
        "subscriber": "11223344"
    },

    CREATE_SUBSCRIBER_ERROR :{
        "ErrorDesc": "Hemos detectado un error en la creación del Suscriptor. Su caso fue enviado al Back End.",
        "ErrorInterInfo": null,
        "ErrorNum": 0,
        "HasError": true,
        "mBan": "790901967",
        "subscriber": ""
    },
    GetLocationMcapi : [
        {
            "description_location": "PLAZA PEREZ HERMANOS",
            "localidad_ID": 1,
            "location_sif": "CBCT"
        },
        {
            "description_location": "TIENDA LOS PRADOS",
            "localidad_ID": 2,
            "location_sif": "CLPC"
        },
        {
            "description_location": "TIENDA SAN GERMAN",
            "localidad_ID": 3,
            "location_sif": "CBSG"
        },
        {
            "description_location": "PLAZA CAPARRA",
            "localidad_ID": 4,
            "location_sif": "CAHR"
        },
        {
            "description_location": "CATALINAS",
            "localidad_ID": 5,
            "location_sif": "CBCM"
        },
        {
            "description_location": "CENTRO DEL SUR",
            "localidad_ID": 6,
            "location_sif": "CBCS"
        },
        {
            "description_location": "PLAZA DEL CARIBE",
            "localidad_ID": 7,
            "location_sif": "CBPO"
        },
        {
            "description_location": "ROOSEVELT 1515",
            "localidad_ID": 8,
            "location_sif": "CBRO"
        },
        {
            "description_location": "PLAZA CENTRO",
            "localidad_ID": 9,
            "location_sif": "CPCC"
        },
        {
            "description_location": "AGUADILLA",
            "localidad_ID": 10,
            "location_sif": "CAGM"
        },
        {
            "description_location": "BARCELONETA",
            "localidad_ID": 11,
            "location_sif": "CLXP"
        },
        {
            "description_location": "CENTRO GRAN CARIBE",
            "localidad_ID": 12,
            "location_sif": "CBVA"
        },
        {
            "description_location": "DORADO DEL MAR",
            "localidad_ID": 13,
            "location_sif": "CTDM"
        },
        {
            "description_location": "ISABELA",
            "localidad_ID": 14,
            "location_sif": "CBAG"
        },
        {
            "description_location": "LOS PALACIOS",
            "localidad_ID": 15,
            "location_sif": "CPAL"
        },
        {
            "description_location": "PLAZA LAS VEGAS",
            "localidad_ID": 16,
            "location_sif": "PLVB"
        },
        {
            "description_location": "UNIVERSITY PLAZA",
            "localidad_ID": 17,
            "location_sif": "MAWP"
        },
        {
            "description_location": "REXVILLE TOWN CENTRE",
            "localidad_ID": 18,
            "location_sif": "MAWP"
        },
        {
            "description_location": "PLAZA DEL SOL",
            "localidad_ID": 19,
            "location_sif": "CTPS"
        },
        {
            "description_location": "BELTZ OUTLET",
            "localidad_ID": 20,
            "location_sif": "CBBF"
        },
        {
            "description_location": "JUNCOS PLAZA",
            "localidad_ID": 21,
            "location_sif": "CJPZ"
        },
        {
            "description_location": "LOS COLOBOS",
            "localidad_ID": 22,
            "location_sif": "CTLC"
        },
        {
            "description_location": "PLAZA ESCORIAL",
            "localidad_ID": 23,
            "location_sif": "COPE"
        },
        {
            "description_location": "CIUDADELA",
            "localidad_ID": 24,
            "location_sif": "CMCS"
        },
        {
            "description_location": "PLAZA CAROLINA",
            "localidad_ID": 25,
            "location_sif": "CTPC"
        },
        {
            "description_location": "TRUJILLO ALTO PLAZA",
            "localidad_ID": 26,
            "location_sif": "CTAP"
        },
        {
            "description_location": "MALL OF SAN JUAN",
            "localidad_ID": 27,
            "location_sif": "CMSJ"
        },
        {
            "description_location": "MERCADO PLAZA",
            "localidad_ID": 28,
            "location_sif": "CMPL"
        },
        {
            "description_location": "PLAZA LAS AMERICAS I",
            "localidad_ID": 29,
            "location_sif": "CBPA"
        },
        {
            "description_location": "CAC FAJARDO",
            "localidad_ID": 30,
            "location_sif": "COFA"
        },
        {
            "description_location": "CAC PLAZA CAROLINA",
            "localidad_ID": 31,
            "location_sif": "CAPC"
        },
        {
            "description_location": "CAC PLAZA CENTRO MALL",
            "localidad_ID": 32,
            "location_sif": "CACC"
        },
        {
            "description_location": "CAC PONCE MALL",
            "localidad_ID": 33,
            "location_sif": "COPO"
        },
        {
            "description_location": "CAC VILLA BLANCA",
            "localidad_ID": 34,
            "location_sif": "COCA"
        },
        {
            "description_location": "CAC PLAZA DEL CARIBE",
            "localidad_ID": 35,
            "location_sif": "CAPO"
        },
        {
            "description_location": "CAC PLAZA GUAYAMA",
            "localidad_ID": 36,
            "location_sif": "CCPG"
        },
        {
            "description_location": "CAC ARECIBO",
            "localidad_ID": 37,
            "location_sif": "COAR"
        },
        {
            "description_location": "CAC CANTÓN MALL",
            "localidad_ID": 38,
            "location_sif": "COBA"
        },
        {
            "description_location": "CAC HATILLO",
            "localidad_ID": 39,
            "location_sif": "CAAR"
        },
        {
            "description_location": "CAC MAYAGÜEZ",
            "localidad_ID": 40,
            "location_sif": "COMA"
        },
        {
            "description_location": "CAC RÍO HONDO",
            "localidad_ID": 41,
            "location_sif": "CABY"
        },
        {
            "description_location": "CAC PLAZA LAS AMERICAS",
            "localidad_ID": 42,
            "location_sif": "CAPA"
        },
        {
            "description_location": "CAC SAN PATRICIO",
            "localidad_ID": 43,
            "location_sif": "COSA"
        }
    ],
    CREATE_ACCOUNT :{
        "ErrorDesc": null,
        "ErrorInterInfo": null,
        "ErrorNum": 0,
        "HasError": false,
        "AccountType": "Prepaid",
        "mBan": "790903981",
        "response": true
    }
}

