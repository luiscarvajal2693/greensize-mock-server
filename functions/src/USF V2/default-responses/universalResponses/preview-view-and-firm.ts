export const PREVIEW_AND_FIRM = {
    //SimpleExtendedResponse
    CREATE_FIRM : {
        "response": false,
        "HasError": false,
        "ErrorDesc": null,
        "ErrorInterInfo": null,
        "ErrorNum": 0
    },
    // exactamente de la misma clase Response
    FccFormDocumentMcapi : {
        ErrorDesc: null,
        ErrorInterInfo: null,
        ErrorNum: 0,
        HasError: false,
    },
    //clase ya creada n modelos
    CREATE_SUBSCRIBER :{
        "ErrorDesc": null,
        "ErrorInterInfo": null,
        "ErrorNum": 0,
        "HasError": false,
        "CUSTOMER_LAST": "RAMOS MARTINEZ",
        "CUSTOMER_MN": "",
        "CUSTOMER_NAME": "JUAN",
        "USF_CASEID": "509",
        "mBan": "790870312",
        "subscriber": "11223344"
    },
}



