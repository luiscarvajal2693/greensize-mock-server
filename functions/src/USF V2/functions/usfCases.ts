// Default data:
import  { CasesDefault }  from '../default-responses/casesDefault';
import { FilterUsfCase } from '../Models/FilterUsfCase'
import { Denied } from '../Models/Denied'
import { EligibilitycheckStatusMcapi } from '../Models/EligibilitycheckStatusMcapi'
import { ModifyBackendId } from '../Models/ModifyBackendId'

export class UsfCases {

    static getCasesWithFiltersMcapi(request: any, response: any) {
        const data = CasesDefault.FILTER_USF_CASE.FILTER_USF_CASE_1
        var defaultResponse : FilterUsfCase;
        defaultResponse = new FilterUsfCase(data.ErrorDesc, null, data.ErrorNum, data.HasError,
                                    data.ErrorType, data.id, data.caseasine, data.customercases,
                                    data.dataVendor, data.pageNum, data.pageSize, data.totalRowCount);

        response.status(200).json(defaultResponse);
    }

    static getCasesdeniedMcapi(request: any, response: any) {

        const data = CasesDefault.DENIED.DENIED_1;
        var defaultResponse : Denied;
        defaultResponse = new Denied(data.ErrorDesc, data.ErrorInterInfo, data.ErrorNum, data.HasError, data.data )

        response.status(200).json(defaultResponse);
    }
    static getCasesModifyBackendId(request: any, response: any) {

        const data = CasesDefault.ModifyBackendId.ModifyBackendId_1;
        var defaultResponse :  ModifyBackendId;
        defaultResponse = new ModifyBackendId( data.ErrorDesc, data.ErrorInterInfo, data.ErrorNum, data.HasError,
                                                data.dataBacken, data.pageNum, data.pageSize, data.totalRowCount)

        response.status(200).json(defaultResponse);
    }

    static eligibilitycheckStatusMcapi(request: any, response: any) {

        const data = CasesDefault.EligibilitycheckStatusMcapi.EligibilitycheckStatusMcapi_1
        var defaultResponse : EligibilitycheckStatusMcapi;
        defaultResponse = new EligibilitycheckStatusMcapi(data.ErrorDesc, data.ErrorInterInfo, data.ErrorNum, data.HasError,
                                                        data.AddValid, data._links, data.activeSubscriber, data.applicationId,
                                                        data.eligibilityCheckId, data.eligibilityExpirationDate, data.errorId,
                                                        data.errors, data.failures, data.lastManualReviewTime, data.message,
                                                        data.numberofManualReview, data.rejections, data.resoncode, data.status,
                                                        data.timestamp,)

        response.status(200).json(defaultResponse);
    }
}
