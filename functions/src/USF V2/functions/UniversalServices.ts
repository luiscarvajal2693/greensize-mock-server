import { UNIVERSALDEFAULTRESPONSE} from '../default-responses/UniversalDefaultResponses';

// Models:
/*
import { AddresDate } from '../Models/AddresDate';
import { CreateAccount } from '../Models/CreateAccount';
import { CreateDepend } from '../Models/CreateDepend';
import { CreateSubscriber } from '../Models/CreateSubscriber';
import { CreateSubcriberError } from '../Models/CreateSubcriberError';
import { Denied } from '../Models/Denied';
import { DigitalizationUploadDocument } from '../Models/DigitalizationUploadDocument';
import { DigitalizationDeleteDocument } from '../Models/DigitalizationDeleteDocument';
import { EligibilitycheckStatusMcapi } from '../Models/EligibilitycheckStatusMcapi';
import { FilterUsfCase } from '../Models/FilterUsfCase';
import { GetBan } from '../Models/GetBan';
import { GetLocationMcapi } from '../Models/GetLocationMcapi';
import { GetLisAgentsMcapi } from '../Models/GetListAgentsMcapi';
import { Login } from '../Models/Login';
import { ModifyBackendId } from '../Models/ModifyBackendId';
import { PersonalDates } from '../Models/PersonalDates';
import { UpdloadDocumentMcapi } from '../Models/UpdloadDocumentMcapi';
import { UsfVerification } from '../Models/UsfVerification';
 */
import { SimpleExtendedResponse } from '../Models/SimpleExtendedResponse';



,
export class AceptationTerms {

    // public AceptationResponse: SimpleExtendedResponse;

    // tslint:disable-next-line:no-empty
    constructor() {}

    public  CreateTerminosMcapi(request: any, response: any) {

        const AceptationResponse : SimpleExtendedResponse = new SimpleExtendedResponse(
                                                                UNIVERSALDEFAULTRESPONSE.TERMS.TERMS_NEGATE.ErrorDesc,
                                                                UNIVERSALDEFAULTRESPONSE.TERMS.TERMS_NEGATE.ErrorInterInfo,
                                                                UNIVERSALDEFAULTRESPONSE.TERMS.TERMS_NEGATE.ErrorNum,
                                                                UNIVERSALDEFAULTRESPONSE.TERMS.TERMS_NEGATE.HasError,
                                                                UNIVERSALDEFAULTRESPONSE.TERMS.TERMS_NEGATE.response
                                                            );

        response.status(200).json(AceptationResponse);
    }

    public Updlongdistance(request: any, response: any) {

        const AceptationResponse : SimpleExtendedResponse = new SimpleExtendedResponse(
                                                                    UNIVERSALDEFAULTRESPONSE.TERMS.TERMS_ACEPTATION.ErrorDesc,
                                                                    UNIVERSALDEFAULTRESPONSE.TERMS.TERMS_ACEPTATION.ErrorInterInfo,
                                                                    UNIVERSALDEFAULTRESPONSE.TERMS.TERMS_ACEPTATION.ErrorNum,
                                                                    UNIVERSALDEFAULTRESPONSE.TERMS.TERMS_ACEPTATION.HasError,
                                                                    UNIVERSALDEFAULTRESPONSE.TERMS.TERMS_ACEPTATION.response
                                                             );

        response.status(200).json(AceptationResponse);
    }
}

export class AccountCreation {
    // tslint:disable-next-line:no-empty
    constructor() {}
    public  CreateNewAccountMcapi(request: any, response: any) {

        response.status(200).json( UNIVERSALDEFAULTRESPONSE.CREATION.CREATE_ACCOUNT);
    }

    public  CreateSubscriberMcapi(request: any, response: any) {
        response.status(200).json(request.body.error === undefined ? UNIVERSALDEFAULTRESPONSE.CREATION.CREATE_SUBSCRIBER : UNIVERSALDEFAULTRESPONSE.CREATION.CREATE_SUBSCRIBER_ERROR);
    }

    public  getLocationMcapi(request: any, response: any) {
        response.status(200).json(UNIVERSALDEFAULTRESPONSE.CREATION.GetLocationMcapi);
    }
}

export class Activation {
    // tslint:disable-next-line:no-empty
    constructor() {}

    public getBanMcapi(request: any, response: any) {
        response.status(200).json(UNIVERSALDEFAULTRESPONSE.ACTIVATION.GET_BAN);
    }
}

export class AddressDate {
    // tslint:disable-next-line:no-empty
    constructor() {}

    public addressValidationMcapi(request: any, response: any) {
        response.status(200).json(UNIVERSALDEFAULTRESPONSE.ADDRESS.ADDRES_DATE);
    }

    public subscriberVerificationMcapi(request: any, response: any) {
        response.status(200).json(UNIVERSALDEFAULTRESPONSE.ADDRESS.USF_VERIFICATION);
    }
}

export class DocumentDigitalization {
    // tslint:disable-next-line:no-empty
    constructor() {}

    public UpdloadDocumentMcapi(request: any, response: any) {
        response.status(200).send( UNIVERSALDEFAULTRESPONSE.DOCUMENT.DIGITALIZATION_UPLOAD_DOCUMENT);
    }

    public DeleteDocumentMcapi(request: any, response: any) {
        response.status(200).json(UNIVERSALDEFAULTRESPONSE.DOCUMENT.DIGITALIZATION_DELETE_DOCUMENT);
    }
}

export class PersonalDates {
    // tslint:disable-next-line:no-empty
    constructor() {}

    public validareSSNAdMcapi(request: any, response: any) {
        response.status(200).json( UNIVERSALDEFAULTRESPONSE.PERSONAL.PERSONAL_DATES);
    }

}

export class PreviewAndFirm {
    // tslint:disable-next-line:no-empty
    constructor() {}

    public CreatefirmMcapi(request: any, response: any) {
        response.status(200).json(UNIVERSALDEFAULTRESPONSE.FIRM.CREATE_FIRM );
    }

    public fccFormDocumentMcapi(request: any, response: any) {
        response.status(200).json(UNIVERSALDEFAULTRESPONSE.FIRM.FccFormDocumentMcapi);
    }
}

export class ResgiterCase {
    // tslint:disable-next-line:no-empty
    constructor() {}

    public deleteCaseAllMcapi(request: any, response: any) {
        response.status(200).json( UNIVERSALDEFAULTRESPONSE.REGISTER.DELETE_CASE);
    }
}

export class UsfVerification {
    // tslint:disable-next-line:no-empty
    constructor() {}

    public createDepend(request: any, response: any) {
        response.status(200).json( UNIVERSALDEFAULTRESPONSE.VERIFICATION.CREATE_DEPEND);
    }
}
