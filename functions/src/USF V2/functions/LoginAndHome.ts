import {LOGIN_CASES} from "../default-responses/loginDataDefault";
import {GETLISTAGENTSMCAPI} from "../default-responses/homeDataDefault";
import { Login } from '../Models/Login'
import { GetLisAgentsMcapi  } from '../Models/GetListAgentsMcapi';
export class LoginAndHome {
    static loginAdMcapi(request: any, response: any) {
        const data = LOGIN_CASES.LOGIN_OK;
        var defaultResponse: Login;
        defaultResponse = new Login(data.ErrorDesc, data.ErrorInterInfo, data.ErrorNum, data.HasError, data.Dealer,
                                  data.Active, data.DealerCode, data.Device_Id, data.Email, data.IsAdmin, data.LastLogin,
                                  data.LastName, data.Location, data.Locations, data.LoginAttems,  data.Name, data.Password,
                                  data.PhoneNumber, data.RoleID, data.RoleName, data.UserName,  data.userid);
        response.status(200).json(defaultResponse);
    }

    static getListAgentMcapi(request: any, response: any) {
        const data = GETLISTAGENTSMCAPI.GETLISTAGENTSMCAPI_OK;
        var defaultResponse : GetLisAgentsMcapi;
        defaultResponse = new GetLisAgentsMcapi(data.ErrorDesc, data.ErrorInterInfo,
                                                data.ErrorNum, data.HasError,
                                                data.LIST_AGENT, data.data);

        response.status(200).json(defaultResponse);
    }
}
